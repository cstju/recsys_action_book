# -*- coding: utf-8 -*-
"""
Created on Wed Apr 13 14:21:20 2016

@author: 3dlabuser
"""
import sys, random, math
from operator import itemgetter


class UserBasedCF():
    ''' TopN recommendation - UserBasedCF '''
    def __init__(self):
        self.trainset = {}#初始化训练集
        self.testset = {}#初始化测试集

        self.n_sim_user = 20#限定相似用户集的个数为20
        self.n_rec_item = 10#限定推荐物品个数为10

        self.item_users = {}
        self.user_sim_mat = {}
        self.item_popular = {}
        self.item_count = 0

        print 'Similar item number = %d' % self.n_sim_user
        print 'Recommended item number = %d' % self.n_rec_item

    def Read_data(self,path,pivot):
        trainset_len = 0
        testset_len = 0
        data = open(path,'r')
        for i,line in enumerate(data):
            user, item, rating, _ = line.split('\t')
            #####################user, movie, rating, _ = line.split('::')
            if i % 100000 == 0:
                print 'loading %s(%s)' % ('data', i)
            if (random.random()<pivot):
                self.trainset.setdefault(user, {})
                self.trainset[user][item] = int(rating)
                trainset_len += 1
            else :
                self.testset.setdefault(user, {})
                self.testset[user][item] = int(rating)
                testset_len += 1
        print 'split training set and test set succ'
        print 'train set = %s' % trainset_len
        print 'test set = %s' % testset_len

    def UserSimilarity(self):
        
        #build inverse table for item_users
        print '开始统计对同一个物品进行过评分的用户集'
        for user,items in self.trainset.iteritems():
            for item in items.keys():
                if item not in self.item_users:
                    self.item_users[item] = set()
                self.item_users[item].add(user)
        print '统计对同一个物品进行过评分的用户集完成'
        

        #calculate co-rated items between users
        print '开始计算用户之间的共同评分数'
        User_rated_num_Matix = {}
        for item,users in self.item_users.iteritems():
            for user in users :
                if user not in User_rated_num_Matix:
                    User_rated_num_Matix[user] = 0
                User_rated_num_Matix[user] += 1
                for related_user in users:
                    if user==related_user:
                        continue
                    self.user_sim_mat.setdefault(user,{})
                    self.user_sim_mat[user].setdefault(related_user,0)
                    self.user_sim_mat[user][related_user] += 1
        print '用户共同评分数矩阵生成完成'

        #calculate finial similarity matix 
        print '开始计算用户相似度矩阵'
        for user,related_users in self.user_sim_mat.iteritems():
            for related_user,co_rated_num in related_users.iteritems():
                #User_rated_num_Matix.setdefault(user, 1)
                #User_rated_num_Matix.setdefault(related_user, 1)
                self.user_sim_mat[user][related_user] = co_rated_num / math.sqrt(
                        User_rated_num_Matix[user]*User_rated_num_Matix[related_user])
        print '用户相似度矩阵计算完成'

    def GetRecommend(self,user,top_N):
        rank = {}
        K = self.n_sim_user
        interacted_items = self.trainset[user]
        for ralated_user,similar in sorted(self.user_sim_mat[user].iteritems(),
                    key = itemgetter(1),reverse=True)[:K]:
            for item,rate in self.trainset[ralated_user].items():
                if item in interacted_items:
                    continue
                rank.setdefault(item,0)
                '''#######################
                这里增加了相似用户对商品的评分,
                但是没有考虑不同用户之间的评分偏好
                ########################'''
                rank[item] += similar*rate
        return sorted(rank.items(), key=itemgetter(1), reverse=True)[:top_N]

    def evaluate(self):
        ''' return precision, recall, coverage and popularity '''
        N = self.n_rec_item
        print 'Evaluation start...'
        hit = 0
        n_precision = 0
        n_recall = 0
        popular_sum = 0
        all_recommend_items = set()
        all_items = set()
        item_popualrity = {}

        for user,items in self.trainset.items():
            for item in items.keys():
                if item not in item_popualrity:
                    item_popualrity[item] = 0
                item_popualrity[item] += 1

        for user in self.trainset.keys():
            test_items = self.testset.get(user, {})
            rank = self.GetRecommend(user,self.n_rec_item)
            for item,pui in rank:
                if item in test_items:
                    hit += 1
                popular_sum += math.log(1+item_popualrity[item])
                all_recommend_items.add(item)    
            n_precision += N
            n_recall += len(test_items)
            for item in self.trainset[user].keys():
                all_items.add(item)
        precision = hit/(1.0*n_precision)
        recall = hit/(1.0*n_recall)
        coverage = len(all_recommend_items)/(1.0*len(all_items))
        #表示推荐商品的流行度，如果推荐的商品都比较热门，那么流行度会比较高
        #如果推荐的商品不是比较热门，那么流行度会比较低
        #在进行推荐的时候应该多推荐不热门产品，提升用户的新颖度，
        #所以衡量推荐系统时，流行度越低越好
        popularity = popular_sum/(1.0*n_precision)
        print 'precision=%.4f\trecall=%.4f\tcoverage=%.4f\tpopularity=%.4f' \
                % (precision, recall, coverage, popularity)



if __name__ == '__main__':
    #################ratingfile = '/Users/3dlabuser/Documents/datasets/MovieLens/MovieLens 1M Dataset/ml-1m/ratings.dat'
    #################ratingfile = '/Users/3dlabuser/Documents/datasets/MovieLens/MovieLens 20M Dataset/ml-20m/ratings.csv'
    ratingfile = '/Users/3dlabuser/Documents/datasets/MovieLens/MovieLens 100K Dataset/ml-100k/u1.base'
    usercf = UserBasedCF()
    usercf.Read_data(ratingfile,0.7)
    usercf.UserSimilarity()
    usercf.evaluate()
    #itemcf.generate_dataset(ratingfile)
    #itemcf.calc_movie_sim()
    #itemcf.evaluate()