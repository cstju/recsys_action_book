# -*- coding: utf-8 -*-
"""
Created on Sat Apr 16 13:36:30 2016

@author: 3dlabuser
"""

import sys, random, math
from operator import itemgetter

class ItemBasedCF():
    ''' TopN recommendation - UserBasedCF '''
    def __init__(self):
        self.trainset = {}#初始化训练集
        self.testset = {}#初始化测试集

        self.n_sim_item = 20#限定相似用户集的个数为20
        self.n_rec_item = 10#限定推荐物品个数为10

        self.item_users = {}
        self.item_sim_mat = {}
        self.item_popular = {}
        self.item_count = 0

        print 'Similar item number = %d' % self.n_sim_item
        print 'Recommended item number = %d' % self.n_rec_item
    def Read_data(self,path,pivot):
        trainset_len = 0
        testset_len = 0
        data = open(path,'r')
        for i,line in enumerate(data):
            user, item, rating, _ = line.split('\t')
            #####################user, movie, rating, _ = line.split('::')
            if i % 100000 == 0:
                print 'loading %s(%s)' % ('data: ', i)
            if (random.random()<pivot):
                self.trainset.setdefault(user, {})
                self.trainset[user][item] = int(rating)
                trainset_len += 1
            else :
                self.testset.setdefault(user, {})
                self.testset[user][item] = int(rating)
                testset_len += 1
        print 'split training set and test set succ'
        print 'train set = %s' % trainset_len
        print 'test set = %s' % testset_len
    def ItemSimilarity(self):
        print '开始计算用户共同评分矩阵'
        for user, items in self.trainset.iteritems():
            for item in items:
                if item not in self.item_popular:
                    self.item_popular[item] = 0
                self.item_popular[item] += 1
                for related_item in items:
                    if item == related_item:
                        continue
                    self.item_sim_mat.setdefault(item,{})
                    self.item_sim_mat[item].setdefault(related_item,0)
                    #len(items)为用户user评分过的商品总数，值越大，对相似度贡献越小
                    self.item_sim_mat[item][related_item] += 1 / math.log(1+len(items)*1.0)
        print '用户共同评分矩阵生成完成'
        print '开始计算用户相似度矩阵'
        for item,related_items in self.item_sim_mat.iteritems():
            for related_item,co_rated_num in related_items.iteritems():
                self.item_sim_mat[item][related_item] = co_rated_num/math.sqrt(
                        self.item_popular[item]*self.item_popular[related_item])
        print '用户相似度矩阵计算完成'
        
    def GetRecommend(self,user,top_N):
        rank = {}
        K = self.n_sim_item
        interacted_items = self.trainset[user]
        for item in interacted_items:
            for related_item,similar in sorted(self.item_sim_mat[item].iteritems(),
                    key = itemgetter(1),reverse=True)[:K]:
                if related_item in interacted_items:
                    continue
                rank.setdefault(related_item,0)
                rank[related_item] += similar
        return sorted(rank.items(), key=itemgetter(1), reverse=True)[:top_N]
        
    def evaluate(self):
        ''' return precision, recall, coverage and popularity '''
        print 'Evaluation start...'
        N = self.n_rec_item
        hit = 0
        n_precision = 0
        n_recall = 0
        popular_sum = 0
        all_recommend_items = set()
        all_items = set()


        for user in self.trainset.keys():
            test_items = self.testset.get(user, {})
            '''aaaaaaaaa'''
            rank = self.GetRecommend(user,N)
            '''aaaaaaaaaa'''
            for item,pui in rank:
                if item in test_items:
                    hit += 1
                popular_sum += math.log(1+self.item_popular[item])
                all_recommend_items.add(item)    
            n_precision += N
            n_recall += len(test_items)
            for item in self.trainset[user].keys():
                all_items.add(item)
        precision = hit/(1.0*n_precision)
        recall = hit/(1.0*n_recall)
        coverage = len(all_recommend_items)/(1.0*len(all_items))
        #表示推荐商品的流行度，如果推荐的商品都比较热门，那么流行度会比较高
        #如果推荐的商品不是比较热门，那么流行度会比较低
        #在进行推荐的时候应该多推荐不热门产品，提升用户的新颖度，
        #所以衡量推荐系统时，流行度越低越好
        popularity = popular_sum/(1.0*n_precision)
        print 'precision=%.4f\trecall=%.4f\tcoverage=%.4f\tpopularity=%.4f' \
                % (precision, recall, coverage, popularity)

if __name__ == '__main__':
    #################ratingfile = '/Users/3dlabuser/Documents/datasets/MovieLens/MovieLens 1M Dataset/ml-1m/ratings.dat'
    #################ratingfile = '/Users/3dlabuser/Documents/datasets/MovieLens/MovieLens 20M Dataset/ml-20m/ratings.csv'
    ratingfile = '/Users/3dlabuser/Documents/datasets/MovieLens/MovieLens 100K Dataset/ml-100k/u1.base'
    itemcf = ItemBasedCF()
    itemcf.Read_data(ratingfile,0.7)
    itemcf.ItemSimilarity()
    itemcf.evaluate()
    #usercf.evaluate()